'use strict';

angular.module('backApp')
	.service('deleteModal', function ($uibModal) {
		return {
			confirm: function(deleteCallBack){
				$uibModal.open({
					templateUrl: 'components/grid/delete.modal.html',
					windowClass: 'modal-danger',
					controller: function($uibModalInstance, $scope){
						$scope.close = function(){
							$uibModalInstance.dismiss('cancel');
						};

						$scope.delete = function(){
							$uibModalInstance.close();
							deleteCallBack();
						};
					}
				});
			}
		};
	});
