'use strict';

describe('Service: deleteModal', function () {

  // load the service's module
  beforeEach(module('backApp'));

  // instantiate service
  var deleteModal;
  beforeEach(inject(function (_deleteModal_) {
    deleteModal = _deleteModal_;
  }));

  it('should do something', function () {
    expect(!!deleteModal).toBe(true);
  });

});
