'use strict';

angular.module('backApp.auth', [
  'backApp.constants',
  'backApp.util',
  'ngCookies',
  'ngRoute'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
