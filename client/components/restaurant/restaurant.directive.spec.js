'use strict';

describe('Directive: restaurant', function () {

  // load the directive's module and view
  beforeEach(module('backApp'));
  beforeEach(module('app/restaurant/restaurant.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<restaurant></restaurant>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the restaurant directive');
  }));
});
