'use strict';

(function() {

function ImageResource($resource) {
	return $resource('/api/images/:id/:controller', { 
		id: '@_id'
		}, {
		update: {
			method: 'POST',
			headers: {'Content-Type': undefined },
			transformRequest: function(data){
				var fd = new FormData();
				fd.append('image', data.image);
				
				return fd;
			}
		}
	});
}

angular.module('backApp.auth')
  .factory('Image', ImageResource);

})();
