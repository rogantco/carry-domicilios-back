'use strict';

(function() {

function MessageResource($resource) {
	return $resource('/api/messages/:id/:controller', {
		id: '@_id'
	});
}

angular.module('backApp')
  .factory('Message', MessageResource);

})();
