'use strict';

class NavbarController {
  //start-non-standard

  isCollapsed = true;
  //end-non-standard

  constructor($location, Auth, Restaurant) {
    var nav = this;

    nav.$location = $location;
    nav.isLoggedIn = Auth.isLoggedIn;
    nav.isAdmin = Auth.isAdmin;
    nav.getCurrentUser = Auth.getCurrentUser;
    nav.Restaurant = Restaurant;

    nav.Restaurant.query(function(restaurants){
      angular.forEach(restaurants, function(restaurant){
        if(restaurant.name === 'Víveres'){
          nav.menu = [{
            'title': 'Restaurantes',
            'link': '/'
          },{
            'title': 'Víveres',
            'link': '/restaurant/' + restaurant._id
          },{
            'title': 'Quienes Somos',
            'link': '/about-us'
          },{
            'title': 'Contactenos',
            'link': '/contact-us'
          }];
        }
      });
    });
  }

  isActive(route) {
    return route === this.$location.path();
  }
}

angular.module('backApp')
  .controller('NavbarController', NavbarController);
