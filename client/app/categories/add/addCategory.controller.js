'use strict';

angular.module('backApp')
	.controller('AddCategoryCtrl', function ($routeParams, Categories, $location) {
		var vm = this;
		vm.categoryId = $routeParams.categoryId;

		if(typeof vm.categoryId === 'undefined'){
			vm.category = {
				name: ''
			};
		}else{
			vm.category = Categories.get({id: vm.categoryId});
		}

		vm.register = function(form) {
			vm.submitted = true;

			if (form.$valid) {
				if(typeof vm.categoryId === 'undefined'){
					var newCategory = new Categories(vm.category);

					newCategory.$save()
					.then(() => {
						$location.path('/admin/categories');
					});
				}else{
					vm.category.$update()
					.then(() => {
						$location.path('/admin/categories');
					});
				}
			}
		};
	});