'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/categories/add', {
        templateUrl: 'app/categories/add/addCategory.html',
        controller: 'AddCategoryCtrl as vm',
        authenticate: 'admin'
      }).when('/admin/categories/edit/:categoryId', {
        templateUrl: 'app/categories/add/addCategory.html',
        controller: 'AddCategoryCtrl as vm',
        authenticate: 'admin'
      });
  });
