'use strict';

(function() {

function CategoriesResource($resource) {
  return $resource('/api/categories/:id/:controller', {
		id: '@_id'
	}, {
		update: {
			method: 'PUT'
		}
	});
}

angular.module('backApp.auth')
  .factory('Categories', CategoriesResource);

})();
