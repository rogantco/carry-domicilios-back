'use strict';

angular.module('backApp')
	.controller('CategoriesCtrl', function (Categories, $location, deleteModal) {
		var vm = this;

		vm.url = $location.path();
		vm.categories = {
			data: Categories.query(), 
			rowHeight: 45,
			columnDefs: [
				{ field: 'name', displayName: 'Nombre', cellTemplate: 'components/grid/name.html' },
				{ field: 'delete', displayName: '', enableSorting: false, cellTemplate: 'components/grid/delete.html' }
			]
		};

		vm.fireDeleteModal = function(id){
			deleteModal.confirm(function(){
				var index = vm.categories.data.map(function(a) {return a._id;}).indexOf(id);
				var restaurant = vm.categories.data[index];

				restaurant.$remove();
				vm.categories.data.splice(index, 1);
			});
		};
	});
