'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/categories', {
        templateUrl: 'app/categories/categories.html',
        controller: 'CategoriesCtrl as vm',
        authenticate: 'admin'
      });
  });
