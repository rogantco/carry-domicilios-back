'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/restaurant/:id', {
        templateUrl: 'app/restaurant/restaurant.html',
        controller: 'RestaurantCtrl as vm'
      });
  });
