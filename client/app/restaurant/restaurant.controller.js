'use strict';

angular.module('backApp')
  .controller('RestaurantCtrl', function ($routeParams, Restaurant, $location, $anchorScroll, Auth, $uibModal, ngDialog, Order) {
    var vm = this;

    vm.gotoAnchor = function(id) {
      var old = $location.hash();
      $location.hash(id);
      $anchorScroll.yOffset = 15;
      $anchorScroll();
      //reset to old to keep any additional routing logic from kicking in
      $location.hash(old);
    }

    vm.orderHelper = {}
    vm.restaurant = Restaurant.get({id: $routeParams.id}, function(){
      vm.orderHelper.order = {
        restaurant: vm.restaurant._id,
        dishes: [],
        paymentMethod: 'Efectivo',
        deliveryCharge: vm.restaurant.deliveryCharge,
        total: vm.restaurant.deliveryCharge
      }
    });

    vm.orderHelper.addToCar = function(menuKey, dishKey){
      if(Auth.isLoggedIn()){
        vm.user = Auth.getCurrentUser();
        vm.orderHelper.order.customer = vm.user._id;
        vm.orderHelper.order.dishes.push({
          name: vm.restaurant.menu[menuKey].dishes[dishKey].name,
          description: vm.restaurant.menu[menuKey].dishes[dishKey].description,
          price: vm.restaurant.menu[menuKey].dishes[dishKey].price
        });
        vm.orderHelper.order.total += vm.restaurant.menu[menuKey].dishes[dishKey].price;
        vm.orderHelper.order.subTotal = vm.orderHelper.order.total - vm.orderHelper.order.deliveryCharge;
        vm.orderHelper.order.deliveryAddress = vm.user.addresses[0];
      }else{
        $location.path('/login');
      }
    }

    vm.orderHelper.removeToCar = function(key){
      vm.orderHelper.order.total -= vm.orderHelper.order.dishes[key].price;
      vm.orderHelper.order.subTotal = vm.orderHelper.order.total - vm.orderHelper.order.deliveryCharge;
      vm.orderHelper.order.dishes.splice(key, 1);
    }

    vm.orderHelper.orderModalToAddress = function(){
      if(vm.orderHelper.order.subTotal >= vm.restaurant.minimumOrder){
        vm.addressModal = $uibModal.open({
          animation: true,
          templateUrl: 'app/restaurant/address.modal.html',
          windowClass: 'modal-primary',
          size: 'lg',
          controller: function($uibModalInstance, $scope, orderHelper, user){
            $scope.orderHelper = orderHelper;
            $scope.user = user;
            $scope.close = function(){
              $uibModalInstance.dismiss('cancel');
            };

            $scope.sendOrder = function(){
              $uibModalInstance.dismiss('cancel');
              orderHelper.sendOrder();
            };
          },
          resolve: {
            orderHelper: function () {
              return vm.orderHelper;
            },
            user: function () {
              return vm.user;
            }
          }
        });

        ga('send', {
          hitType: 'event',
          eventCategory: 'Order',
          eventAction: 'Open addresses modal',
        });
      }else{
        ngDialog.open({
          template: 'app/restaurant/minimum.modal.html',
          className: 'ngdialog-theme-default'
        });

        ga('send', {
          hitType: 'event',
          eventCategory: 'Order',
          eventAction: 'Open minimum modal',
        });
      }
    }

    vm.orderHelper.selectedAddresChange = function(){
      if(vm.orderHelper.selectedAddres !== ''){
        vm.orderHelper.order.deliveryAddress = vm.user.addresses[vm.orderHelper.selectedAddres];
      }
    }

    vm.orderHelper.sendOrder = function(){
      vm.order = new Order(vm.orderHelper.order);
      vm.order.$save().then(function(order) {
        ngDialog.open({
          template:'\
              <p>Tu pedido fue enviado exitosamente, pronto recibiras la confirmación.</p>\
              <div class="ngdialog-buttons">\
                  <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(0)">Ok</button>\
              </div>',
          plain: true
        }).closePromise.then(function(){
          $location.path('/');
        });

        ga('send', {
          hitType: 'event',
          eventCategory: 'Order',
          eventAction: 'Confirm',
        });

        ga('require', 'ec');

        angular.forEach(vm.order.dishes, function(dish) {
          ga('ec:addProduct', {
            'id': dish._id,
            'name': dish.name,
            //'category': '',
            'brand': vm.restaurant.name,
            'price': dish.price,
            'quantity': 1
          });
        });

        ga('ec:setAction', 'purchase', {
          'id': vm.order._id,
          'affiliation': vm.restaurant.name,
          'revenue': vm.order.subTotal,
          'tax': 0,
          'shipping': vm.order.deliveryCharge,
        });

      }).catch(function(err){
        ngDialog.open({
          template:'\
              <p>Tu pedido no fue enviado por favor intentalo de nuevo.</p>\
              <div class="ngdialog-buttons">\
                  <button type="button" class="ngdialog-button ngdialog-button-danger" ng-click="confirm(1)">Ok</button>\
              </div>',
          plain: true
        });

        ga('send', {
          hitType: 'event',
          eventCategory: 'Order',
          eventAction: 'Fail',
        });
      });
    }
  });
