'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/users/add', {
        templateUrl: 'app/users/add/add.html',
        controller: 'AddCtrl as vm',
        authenticate: 'admin'
      }).when('/admin/users/edit/:userId', {
        templateUrl: 'app/users/add/add.html',
        controller: 'AddCtrl as vm',
        authenticate: 'admin'
      });
  });
