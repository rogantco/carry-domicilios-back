'use strict';

angular.module('backApp')
  .controller('AddCtrl', function (appConfig, Auth, $location, $routeParams, User) {
	var vm = this;

	vm.userId = $routeParams.userId;	
	vm.streetTypes = appConfig.streetTypes;

	if(typeof vm.userId === 'undefined'){
		vm.user = {
			addresses: [
				{
					description: '',
					streetType: '',
					num1: '',
					num2: '',
					num3: '',
					address2: '',
					phone: '',
					featured: false
				}
			]
		};
	}else{
		vm.user = User.get({id: vm.userId});
	}

	vm.register = function(form) {
		vm.submitted = true;

		if (form.$valid) {
			if(typeof vm.userId === 'undefined'){
				Auth.createUser(vm.user)
				.then(() => {
					// Account created, redirect to home
					$location.path('/admin/users');
				})
				.catch(err => {
					err = err.data;
					vm.errors = {};

					// Update validity of form fields that match the mongoose errors
					angular.forEach(err.errors, (error, field) => {
						form[field].$setValidity('mongoose', false);
						vm.errors[field] = error.message;
					});
				});
			}else{
				vm.user.$update()
				.then(() => {
					// Account created, redirect to home
					$location.path('/admin/users');
				})
				.catch(err => {
					err = err.data;
					vm.errors = {};

					// Update validity of form fields that match the mongoose errors
					angular.forEach(err.errors, (error, field) => {
						form[field].$setValidity('mongoose', false);
						vm.errors[field] = error.message;
					});
				});				
			}
		}
	};

	vm.addAddress = function(){
		vm.user.addresses.push({
			description: '',
			streetType: '',
			num1: '',
			num2: '',
			num3: '',
			address2: '',
			phone: '',
			featured: false
		});
	};

	vm.removeAddress = function(key){
		vm.user.addresses.splice(key, 1);

		if(vm.user.addresses.length === 0){
			vm.addAddress();
		}
	};	
  });