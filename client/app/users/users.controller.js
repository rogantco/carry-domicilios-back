'use strict';

angular.module('backApp')
	.controller('UsersCtrl', function (User, deleteModal, $location) {
		var vm = this;

		vm.url = $location.path();
		vm.users = {
			data: User.query(), 
			rowHeight: 45,
			columnDefs: [
				{ field: 'name', displayName: 'Nombre', cellTemplate: 'components/grid/name.html' },
				{ field: 'email', displayName: 'Email' },
				{ field: 'role', displayName: 'Rol'},
				{ field: 'delete', displayName: '', enableSorting: false, cellTemplate: 'components/grid/delete.html' }
			]
		};

		vm.fireDeleteModal = function(id){
			deleteModal.confirm(function(){
				var index = vm.users.data.map(function(a) {return a._id;}).indexOf(id);
				var user = vm.users.data[index];

				user.$remove();
				vm.users.data.splice(index, 1);
			});
		};		
	});