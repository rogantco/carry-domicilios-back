'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/users', {
        templateUrl: 'app/users/users.html',
        controller: 'UsersCtrl as vm',
        authenticate: 'admin'
      });
  });
