'use strict';

(function() {

class MainController {

  constructor($http, Restaurant, Categories) {
    this.$http = $http;
    this.restaurants = Restaurant.query();
    this.categories = Categories.query();
    this.currentCategory;
  }

  filterRestaurants(category){
    this.currentCategory = category;
  }
}

angular.module('backApp')
  .controller('MainController', MainController);

})();
