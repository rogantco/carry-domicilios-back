'use strict';

angular.module('backApp', [
  'backApp.auth',
  'backApp.admin',
  'backApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap',
  'validation.match',
  'ui.grid',
  'ui.grid.pagination',
  'ng-currency',
  'angularSpinner',
  'sticky',
  'ngDialog',
  'vcRecaptcha'
])
  .config(function($routeProvider, $locationProvider) {
    $routeProvider
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
  });
