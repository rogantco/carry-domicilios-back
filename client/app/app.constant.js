(function(angular, undefined) {
'use strict';

angular.module('backApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin'],streetTypes:['Calle','Carrera','Avenida','Avenida Carrera','Avenida Calle','Circular','Circunvalar','Diagonal','Manzana','Transversal','Vía']})

;
})(angular);