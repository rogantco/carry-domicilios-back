'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/contact-us', {
        templateUrl: 'app/contact-us/contact-us.html',
        controller: 'ContactUsCtrl as vm'
      });
  });
