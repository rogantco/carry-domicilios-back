'use strict';

angular.module('backApp')
  .controller('ContactUsCtrl', function (Message, ngDialog, $location) {
    var vm = this;
    vm.contactForm = {};

    vm.contact = function(form){
      vm.submitted = true;

      if (form.$valid && vm.contactForm.recaptcha) {
        var message = new Message(vm.contactForm);
        message.$save().then(function(){
          ngDialog.open({
            template:'\
                <p>Tu mensaje enviado exitosamente!</p>\
                <div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(0)">Ok</button>\
                </div>',
            plain: true
          }).closePromise.then(function(){
            $location.path('/');
          });
        });
      }
    };
  });
