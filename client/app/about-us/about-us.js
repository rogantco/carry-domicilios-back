'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/about-us', {
        templateUrl: 'app/about-us/about-us.html',
        controller: 'AboutUsCtrl'
      });
  });
