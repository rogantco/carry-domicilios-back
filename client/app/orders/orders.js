'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/orders', {
        templateUrl: 'app/orders/orders.html',
        controller: 'OrdersCtrl as vm',
        authenticate: 'admin'
      });
  });
