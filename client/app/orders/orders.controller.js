'use strict';

angular.module('backApp')
	.controller('OrdersCtrl', function (Order, $location) {
		var vm = this;

		vm.url = $location.path();
		vm.orders = {
			data: Order.query(), 
			columnDefs: [
				{ field: '_id', displayName: 'ID', cellTemplate: 'components/grid/name.html' },
				{ field: 'restaurant.name', displayName: 'Restaurante' },
				{ field: 'customer.name', displayName: 'Cliente' },
				{ field: 'createdAt', displayName: 'Fecha', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity[col.field] | date:"MM/dd/yyyy h:mma"}}</div>' },
				{ field: 'status', displayName: 'Estatus', enableSorting: false }
			]
		};		
	});
