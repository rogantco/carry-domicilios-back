'use strict';

(function() {

function OrderResource($resource) {
	return $resource('/api/orders/:id/:controller', {
		id: '@_id'
	}, {
		update: {
			method: 'PUT'
		},
		byUser: {
			method: 'GET',
			isArray: true,
			params: {
				controller:'user'
			}
		}
	});
}

angular.module('backApp.auth')
  .factory('Order', OrderResource);

})();
