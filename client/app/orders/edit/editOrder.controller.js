'use strict';

angular.module('backApp')
	.controller('EditOrderCtrl', function (Order, $routeParams, $location) {
		var vm = this;
		vm.orderId = $routeParams.orderId;
		vm.order = Order.get({id: vm.orderId});

		vm.update = function(){
			vm.order.$update()
			.then(() => {
				$location.path('/admin/orders');
			});
		};
	});
