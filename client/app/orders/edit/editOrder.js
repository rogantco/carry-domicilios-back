'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/orders/edit/:orderId', {
        templateUrl: 'app/orders/edit/editOrder.html',
        controller: 'EditOrderCtrl as vm',
        authenticate: 'admin'
      });
  });
