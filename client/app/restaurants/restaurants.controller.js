'use strict';

angular.module('backApp')
	.controller('RestaurantsCtrl', function (Restaurant, $location, deleteModal) {
		var vm = this;

		vm.url = $location.path();
		vm.restaurants = {
			data: Restaurant.query(), 
			rowHeight: 45,
			columnDefs: [
				{ field: 'name', displayName: 'Nombre', cellTemplate: 'components/grid/name.html' },
				{ field: 'category.name', displayName: 'Categoria' },
				{ field: 'nit', enableSorting: false },
				{ field: 'phone', displayName: 'Teléfono', enableSorting: false },
				{ field: 'delete', displayName: '', enableSorting: false, cellTemplate: 'components/grid/delete.html' }
			]
		};

		vm.fireDeleteModal = function(id){
			deleteModal.confirm(function(){
				var index = vm.restaurants.data.map(function(a) {return a._id;}).indexOf(id);
				var restaurant = vm.restaurants.data[index];

				restaurant.$remove();
				vm.restaurants.data.splice(index, 1);
			});
		};

	});