'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/restaurants', {
        templateUrl: 'app/restaurants/restaurants.html',
        controller: 'RestaurantsCtrl as vm',
        authenticate: 'admin'
      });
  });
