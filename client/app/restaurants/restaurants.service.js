'use strict';

(function() {

function RestaurantResource($resource) {
	return $resource('/api/restaurants/:id/:controller', {
		id: '@_id'
	}, {
		update: {
			method: 'PUT'
		}
	});
}

angular.module('backApp.auth')
  .factory('Restaurant', RestaurantResource);

})();
