'use strict';

angular.module('backApp')
.controller('AddRestaurantCtrl', function (appConfig, Categories, $location, Restaurant, Image, usSpinnerService, $routeParams) {
	var vm = this;

	vm.restaurantId = $routeParams.restaurantId;

	vm.streetTypes = appConfig.streetTypes;
	vm.categories = Categories.query();

	if(typeof vm.restaurantId === 'undefined'){
		vm.restaurant = {
			name: '',
			nit: '',
			active: true,
			coordinates: [0, 0], 
			address: {
				streetType: '',
				num1: '',
				num2: '',
				num3: '',
				address2: ''
			},
			phone: '',
			email: '',
			paymentMethods: {
				cash: true,
				creditCard: false			
			},
			deliveryCharge: 2000,
			deliveryTime: 30,
			minimumOrder: 10000,
			category: '',
			//logo: '',
			featured: false,
			menu: [
				{
					title: '',
					dishes: [
						{
							name: '',
							description: '',
							//img: '',
							price: 0
						}
					]
				}
			]
		};
	}else{
		vm.restaurant = Restaurant.get({id: vm.restaurantId});
	}


	vm.uploadImages = function(){
		var promise = new Promise(function (resolve, reject){
			var filesPromises = [];

			if(typeof angular.element('input[name=logo]')[0].files[0] !== 'undefined'){
				var newImage = new Image({image: angular.element('input[name=logo]')[0].files[0]});
				var newImagePromise = newImage.$update()
				.then((data) => {
					vm.restaurant.logo = data._id;
				});

				filesPromises.push(newImagePromise);
			}

			angular.forEach(vm.restaurant.menu, function(menu, key){
				angular.forEach(menu.dishes, function(dish, keyD){
					if(typeof angular.element('input[name=img'+key+keyD+']')[0].files[0] !== 'undefined'){
						var newImage = new Image({image: angular.element('input[name=img'+key+keyD+']')[0].files[0]});
						var newImagePromise = newImage.$update()
						.then((data) => {
							dish.img = data._id;
						});

						filesPromises.push(newImagePromise);
					}
				});
			});
			
			Promise.all(filesPromises).then(function() { 
				resolve();
			}, function(){
				reject();
			});
		}); 

		return promise;
	};

	vm.register = function(form) {
		vm.submitted = true;

		if (form.$valid) {
			usSpinnerService.spin('spinner-1');

			vm.uploadImages().then(function(){
				if(typeof vm.restaurantId === 'undefined'){
					var newRestaurant = new Restaurant(vm.restaurant);

					newRestaurant.$save()
					.then(() => {
						$location.path('/admin/restaurants');
					})
					.catch(err => {
						err = err.data;
						vm.errors = {};

						// Update validity of form fields that match the mongoose errors
						angular.forEach(err.errors, (error, field) => {
							form[field].$setValidity('mongoose', false);
							vm.errors[field] = error.message;
						});
					});
				}else{
					vm.restaurant.$update()
					.then(() => {
						$location.path('/admin/restaurants');
					})
					.catch(err => {
						err = err.data;
						vm.errors = {};

						// Update validity of form fields that match the mongoose errors
						angular.forEach(err.errors, (error, field) => {
							form[field].$setValidity('mongoose', false);
							vm.errors[field] = error.message;
						});
					});
				}
			});
		}
	};

	vm.addMenu = function(){
		vm.restaurant.menu.push({
			title: '',
			dishes: [
				{
					name: '',
					description: '',
					//img: '',
					price: 0
				}
			]
		});
	};

	vm.removeMenu = function(key){
		vm.restaurant.menu.splice(key, 1);

		if(vm.restaurant.menu.length === 0){
			vm.addMenu();
		}
	};

	vm.addDish = function(key){
		vm.restaurant.menu[key].dishes.push({
			name: '',
			description: '',
			//img: '',
			price: 0
		});
	};

	vm.removeDish = function(key, keyD){
		vm.restaurant.menu[key].dishes.splice(keyD, 1);

		if(vm.restaurant.menu[key].dishes.length === 0){
			vm.addDish(key);
		}
	};
});
