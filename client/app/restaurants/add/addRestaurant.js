'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/restaurants/add', {
        templateUrl: 'app/restaurants/add/addRestaurant.html',
        controller: 'AddRestaurantCtrl as vm',
        authenticate: 'admin'
      }).when('/admin/restaurants/edit/:restaurantId', {
        templateUrl: 'app/restaurants/add/addRestaurant.html',
        controller: 'AddRestaurantCtrl as vm',
        authenticate: 'admin'
      });
  });
