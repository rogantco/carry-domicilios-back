'use strict';

describe('Controller: AddRestaurantCtrl', function () {

  // load the controller's module
  beforeEach(module('backApp'));

  var AddRestaurantCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddRestaurantCtrl = $controller('AddRestaurantCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
