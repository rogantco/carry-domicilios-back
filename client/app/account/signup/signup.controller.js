'use strict';

class SignupController {
  //start-non-standard
  user = {
    addresses: []
  };
  errors = {};
  submitted = false;
  //end-non-standard

  constructor(Auth, $location, appConfig, $cookies) {
    this.Auth = Auth;
    this.$location = $location;
    this.streetTypes = appConfig.streetTypes;
  }

  register(form) {
    this.submitted = true;

    if (form.$valid) {
      this.Auth.createUser(this.user, null, true)
      .then(() => {
        // Account created, redirect to home
        this.$location.path('/');
      })
      .catch(err => {
        err = err.data;
        this.errors = {};

        // Update validity of form fields that match the mongoose errors
        angular.forEach(err.errors, (error, field) => {
          form[field].$setValidity('mongoose', false);
          this.errors[field] = error.message;
        });
      });
    }
  }
}

angular.module('backApp')
  .controller('SignupController', SignupController);
