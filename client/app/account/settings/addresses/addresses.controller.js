'use strict';

angular.module('backApp')
  .controller('AddressesCtrl', function (Auth, $location, appConfig, ngDialog) {
    var vm = this;
    vm.streetTypes = appConfig.streetTypes;
    vm.user = Auth.getCurrentUser();

    vm.addAddress = function(){
      vm.user.addresses.push({
        description: '',
        streetType: '',
        num1: '',
        num2: '',
        num3: '',
        address2: '',
        phone: '',
        featured: false
      });
    };

    vm.removeAddress = function(key){
      vm.user.addresses.splice(key, 1);

      if(vm.user.addresses.length === 0){
        vm.addAddress();
      }
    };

    vm.updateUser = function(form){
      vm.submitted = true;

      if (form.$valid) {    
        vm.user.$update()
          .then(function() {
            ngDialog.open({
              template:'\
                  <p>Tu dirección fue actualizada exitosamente.</p>\
                  <div class="ngdialog-buttons">\
                      <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(0)">Ok</button>\
                  </div>',
              plain: true
            }).closePromise.then(function(){
              $location.path('/');
            });
          })
          .catch(function(err) {
            err = err.data;
            vm.errors = {};

            // Update validity of form fields that match the mongoose errors
            angular.forEach(err.errors, function(error, field) {
              form[field].$setValidity('mongoose', false);
              vm.errors[field] = error.message;
            });
          });
      }
    }
  });
