'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/settings/addresses', {
        templateUrl: 'app/account/settings/addresses/addresses.html',
        controller: 'AddressesCtrl as vm',
        authenticate: true
      });
  });
