'use strict';

angular.module('backApp')
  .controller('SettingsOrdersCtrl', function (Auth, Order) {
    var vm = this;

    Auth.getCurrentUser().$promise.then(function(user) {
      vm.user = user;
      vm.orders = Order.byUser({id: vm.user._id});
    });
  });
