'use strict';

angular.module('backApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/settings/orders', {
        templateUrl: 'app/account/settings/orders/orders.html',
        controller: 'SettingsOrdersCtrl as vm',
        authenticate: true
      });
  });
