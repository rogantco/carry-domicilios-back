'use strict';

class PasswordController {
  //start-non-standard
  errors = {};
  submitted = false;
  //end-non-standard

  constructor(Auth, ngDialog, $location) {
    this.Auth = Auth;
    this.ngDialog = ngDialog;
  }

  changePassword(form) {
    this.submitted = true;

    if (form.$valid) {
      this.Auth.changePassword(this.user.oldPassword, this.user.newPassword)
        .then(() => {
          this.ngDialog.open({
            template:'\
                <p>Tu contraseña fue actualizada exitosamente</p>\
                <div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(0)">Ok</button>\
                </div>',
            plain: true
          });
        })
        .catch(() => {
          form.password.$setValidity('mongoose', false);
          this.errors.other = 'Contraseña incorrecta';
          this.message = '';
        });
    }
  }
}

angular.module('backApp')
  .controller('PasswordController', PasswordController);
