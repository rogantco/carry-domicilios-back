'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/back-dev'
  },

  // Seed database on startup
  seedDB: true,
  uploadsPath:  process.env.UPLOADS_PATH ||
                '/Volumes/Data/Dev/carry/back/uploads'
};
