'use strict';

exports = module.exports = {
	// List of user roles
	userRoles: ['guest', 'user', 'admin'],
	streetTypes: [
		'Calle',
		'Carrera',
		'Avenida',
		'Avenida Carrera',
		'Avenida Calle',
		'Circular',
		'Circunvalar',
		'Diagonal',
		'Manzana',
		'Transversal',
		'Vía'
	]
};
