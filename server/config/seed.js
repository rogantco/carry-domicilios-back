/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import User from '../api/user/user.model';
import Restaurant from '../api/restaurant/restaurant.model';
import Categories from '../api/categories/categories.model';
import Order from '../api/order/order.model';
import Image from '../api/image/image.model';


var deleteDataPromises = [];

var pd1 = User.find({}).removeAsync();
deleteDataPromises.push(pd1);

var pd2 = Categories.find({}).removeAsync();
deleteDataPromises.push(pd2);

var pd3 = Image.find({}).removeAsync();
deleteDataPromises.push(pd3);

var pd4 = Restaurant.find({}).removeAsync();
deleteDataPromises.push(pd4);

Promise.all(deleteDataPromises).then(function(result) {
  populate();
});


function populate(){
  //Populate Categories
  Categories.create({
    name: 'Comida Rapida'
  }, {
    name: 'Pizza'
  }, {
    name: 'Tipica'
  }, {
    name: 'Mediterránea'
  });


  //Populate supermarket
  var supermarketPromise = []

  var vp1 = Categories.create({
    name: 'Víveres'
  })
  supermarketPromise.push(vp1);

  var vp2 = Image.create({
    path: 'supermarket.jpg'
  })
  supermarketPromise.push(vp2);

  var vp3 = Image.create({
    path: 'tomate_cherry_2.gif'
  })
  supermarketPromise.push(vp3);

  Promise.all(supermarketPromise).then(function(result) {
    populatesupermarket(result[0], result[1], result[2]);
  });


  var orderPromises = []
  //Populate Users
  orderPromises.push(populateUsers());

  
  //Populate Restaurant 1
  var restaurant1Promises = [];

  var rp1 = Categories.create({
    name: 'Pollo'
  })
  restaurant1Promises.push(rp1);

  var rp2 = Image.create({
    path: 'moesLogo.png'
  })
  restaurant1Promises.push(rp2);

  var rp3 = Image.create({
    path: 'pollo4.jpg'
  });
  restaurant1Promises.push(rp3);

  Promise.all(restaurant1Promises).then(function(result) {
    orderPromises.push(populateRestaurantOne(result[0], result[1], result[2]));

    Promise.all(orderPromises).then(function(result) {
      populateOrders(result[1], result[0][0])
    });
  });

  //Populate Restaurant 2
  var restaurant2Promises = [];

  var r2p1 = Categories.create({
    name: 'Italiana'
  })
  restaurant2Promises.push(r2p1);

  var r2p2 = Image.create({
    path: 'archies_logo_0.ai__0.png'
  })
  restaurant2Promises.push(r2p2);

  var r2p3 = Image.create({
    path: 'huerto.png'
  });
  restaurant2Promises.push(r2p3); 

  var r2p4 = Image.create({
    path: 'archies.png'
  });
  restaurant2Promises.push(r2p4);

  var r2p5 = Image.create({
    path: 'salad.jpg'
  });
  restaurant2Promises.push(r2p5);

  Promise.all(restaurant2Promises).then(function(result) {
    populateRestaurantTwo(result[0], result[1], result[2], result[3], result[4]);
  });

  //Populate Restaurant 3
  var restaurant3Promises = [];

  var r3p1 = Categories.create({
    name: 'Asiatica'
  })
  restaurant3Promises.push(r3p1);

  var r3p2 = Image.create({
    path: 'wok.png'
  })
  restaurant3Promises.push(r3p2);

  var r3p3 = Image.create({
    path: 'ramen.png'
  });
  restaurant3Promises.push(r3p3);

  Promise.all(restaurant3Promises).then(function(result) {
    populateRestaurantThree(result[0], result[1], result[2]);
  });  
}

function populateUsers(){
  return User.createAsync({
    provider: 'local',
    name: 'Pepito Vargas',
    email: 'rogant+domi@gmail.com',
    password: 'test',
    addresses: [{
      description: 'Apto',
      streetType: 'Calle',
      num1: '163B',
      num2: '50',
      num3: '80',
      address2: 'Apto 208',
      phone: '3192396517',
      featured: true
    }]
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Andres Vargas',
    email: 'rogant@gmail.com',
    password: 'admin'
  });
}

function populatesupermarket(category, logo, dish){
  return Restaurant.create({
    name: 'Víveres',
    nit: '45634563456',
    active: true,
    coordinates: [4.746072, -74.052401], 
    address: {
      streetType: 'Calle',
      num1: '30',
      num2: '50 sur',
      num3: '20',
      address2: 'Local 10'
    },
    phone: '3192396517',
    email: 'rogant+re@gmail.com',
    paymentMethods: {
      cash: true,
      creditCard: true
    },
    deliveryCharge: 2000,
    deliveryTime: 50,
    minimumOrder: 10000,
    category: category._id,
    logo: logo._id,
    featured: false,
    menu: [
      {
        title: 'Verduras',
        dishes: [
          {
            name: 'Tomate Cherry',
            description: '1 Libra',
            img: dish._id,
            price: 2300
          }
        ]
      }
    ],
    workingTime:{
      monday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      tuesday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      wednesday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      thursday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 23, 0)
      },
      friday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      saturday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      sunday: {
        open: new Date(1989, 5, 13, 0, 0),
        closed: new Date(1989, 5, 13, 0, 0)
      }
    },
  });
}

function populateRestaurantOne(category, logo, dish){
  return Restaurant.create({
    name: 'Moes\'s',
    nit: '456667978',
    active: true,
    coordinates: [4.746072, -74.052401], 
    address: {
      streetType: 'Calle',
      num1: '163B',
      num2: '50',
      num3: '80',
      address2: 'Local 1'
    },
    phone: '3192396517',
    email: 'rogant+re@gmail.com',
    paymentMethods: {
      cash: true,
      creditCard: true
    },
    deliveryCharge: 2000,
    deliveryTime: 50,
    minimumOrder: 20000,
    category: category._id,
    logo: logo._id,
    featured: false,
    menu: [
      {
        title: 'Combos',
        dishes: [
          {
            name: '1 Pollo',
            description: '1 Pollo con papas',
            img: dish._id,
            price: 22000
          }
        ]
      }
    ],
    workingTime:{
      monday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      tuesday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      wednesday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      thursday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      friday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      saturday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      sunday: {
        open: new Date(1989, 5, 13, 0, 0),
        closed: new Date(1989, 5, 13, 0, 0)
      }
    },
  });
}

function populateRestaurantTwo(category, logo, dish1, dish2, dish3){
  return Restaurant.create({
    name: 'Archie\'s',
    nit: '7887666565',
    active: true,
    coordinates: [4.746072, -74.052401], 
    address: {
      streetType: 'Calle',
      num1: '12a',
      num2: '76',
      num3: '34',
      address2: 'Local 23'
    },
    phone: '5672345',
    email: 'rogant+re@gmail.com',
    paymentMethods: {
      cash: true,
      creditCard: false
    },
    deliveryCharge: 2500,
    deliveryTime: 60,
    minimumOrder: 20000,
    category: category._id,
    logo: logo._id,
    featured: false,
    menu: [
      {
        title: 'Pizza',
        dishes: [
          {
            name: 'PIZZA HUERTO®',
            description: 'Variedad de quesos frescos y maduros, ensalada de alcachofa, rúgula, jamón y champiñones en salsa césar.',
            img: dish1._id,
            price: 20000
          },
          {
            name: 'PIZZA ARCHIE\'S®',
            description: 'Prosciutto, pepperoni, salami y jamón',
            img: dish2._id,
            price: 20000
          }              
        ]
      },
      {
        title: 'Ensaladas',
        dishes: [
          {
            name: 'ENSALADA DEL CHEF',
            description: 'Variedad de lechugas, champiñón, espárragos, mazorquitas, huevo, pollo, jamón, aceitunas y queso mozzarella. Queso azul.',
            img: dish3._id,
            price: 25000
          },
          {
            name: 'ENSALADA VERDE',
            description: 'Variedad de lechugas, champiñones, portobellos asados con cebolla caramelizada, tomate cherry, maíz, queso mozzarella y crutones.',
            img: dish3._id,
            price: 20900
          }              
        ]
      }          
    ],
    workingTime:{
      monday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 23, 30)
      },
      tuesday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 23, 30)
      },
      wednesday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 23, 30)
      },
      thursday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 23, 30)
      },
      friday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 23, 30)
      },
      saturday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 23, 30)
      },
      sunday: {
        open: new Date(1989, 5, 13, 0, 0),
        closed: new Date(1989, 5, 13, 0, 0)
      }
    },
  });
}

function populateRestaurantThree(category, logo, dish){
  return Restaurant.create({
    name: 'WOK',
    nit: '6786786768',
    active: true,
    coordinates: [4.746072, -74.052401], 
    address: {
      streetType: 'Calle',
      num1: '70',
      num2: '13',
      num3: '80',
      address2: 'Local 3234'
    },
    phone: '3192396517',
    email: 'rogant+re@gmail.com',
    paymentMethods: {
      cash: true,
      creditCard: false
    },
    deliveryCharge: 2000,
    deliveryTime: 50,
    minimumOrder: 14000,
    category: category._id,
    logo: logo._id,
    featured: false,
    menu: [
      {
        title: 'Ramen y Sopas',
        dishes: [
          {
            name: 'WOK Ramen',
            description: 'Tallarines con Huevo',
            img: dish._id,
            price: 22000
          }
        ]
      }
    ],
    workingTime:{
      monday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      tuesday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      wednesday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      thursday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      friday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      saturday: {
        open: new Date(1989, 5, 13, 8, 0),
        closed: new Date(1989, 5, 13, 18, 0)
      },
      sunday: {
        open: new Date(1989, 5, 13, 0, 0),
        closed: new Date(1989, 5, 13, 0, 0)
      }
    },
  });  
}

function populateOrders(restaurant, user){
  //Orders
  Order.find({}).removeAsync()
    .then(() => {
      Order.create({
        restaurant: restaurant._id,
        customer: user._id,
        deliveryAddress: {
          streetType: 'Calle',
          num1: '163B',
          num2: '50',
          num3: '80',
          address2: 'Apto 208',
          phone: '3194445555'
        },
        dishes: [
          {
            name: '1 Pollo',
            price: 22000
          }
        ],
        paymentMethod: 'Efectivo',
        deliveryCharge: 2000,
        total: 24000,
        subTotal: 22000
      }).then((result) => {
        console.log('finished populating orders');
      });
    });
}