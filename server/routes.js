/**
 * Main application routes
 */

'use strict';

import express from 'express';
import errors from './components/errors';
import path from 'path';
import config from './config/environment';

export default function(app) {
  // Insert routes below
  app.use('/api/messages', require('./api/message'));
  app.use('/api/orders', require('./api/order'));
  app.use('/api/images', require('./api/image'));
  app.use('/api/categories', require('./api/categories'));
  app.use('/api/restaurants', require('./api/restaurant'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth'));

  //static routes
  app.use('/static/images', express.static(config.uploadsPath));


  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });
}
