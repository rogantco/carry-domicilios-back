'use strict';

var app = require('../..');
import request from 'supertest';

var newRestaurant;

describe('Restaurant API:', function() {

  describe('GET /api/restaurants', function() {
    var restaurants;

    beforeEach(function(done) {
      request(app)
        .get('/api/restaurants')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          restaurants = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      restaurants.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/restaurants', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/restaurants')
        .send({
          name: 'New Restaurant'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newRestaurant = res.body;
          done();
        });
    });

    it('should respond with the newly created restaurant', function() {
      newRestaurant.name.should.equal('New Restaurant');
    });

  });

  describe('GET /api/restaurants/:id', function() {
    var restaurant;

    beforeEach(function(done) {
      request(app)
        .get('/api/restaurants/' + newRestaurant._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          restaurant = res.body;
          done();
        });
    });

    afterEach(function() {
      restaurant = {};
    });

    it('should respond with the requested restaurant', function() {
      restaurant.name.should.equal('New Restaurant');
    });

  });

  describe('PUT /api/restaurants/:id', function() {
    var updatedRestaurant;

    beforeEach(function(done) {
      request(app)
        .put('/api/restaurants/' + newRestaurant._id)
        .send({
          name: 'Updated Restaurant',
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedRestaurant = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedRestaurant = {};
    });

    it('should respond with the updated restaurant', function() {
      updatedRestaurant.name.should.equal('Updated Restaurant');
    });

  });

  describe('DELETE /api/restaurants/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/restaurants/' + newRestaurant._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when restaurant does not exist', function(done) {
      request(app)
        .delete('/api/restaurants/' + newRestaurant._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
