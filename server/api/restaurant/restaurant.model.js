'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var Schema = mongoose.Schema;
var RestaurantSchema = new mongoose.Schema({
	name: String,
	nit: String,
	active: Boolean,
	coordinates: {
		type: [Number],
		index: '2dsphere'
	}, 
	address: {
		streetType: String,
		num1: String,
		num2: String,
		num3: String,
		address2: String
	},
	phone: String,
	email: String,
	paymentMethods: {
		cash: Boolean,
		creditCard: Boolean
	},
	deliveryCharge: Number,
	deliveryTime: Number,
	minimumOrder: Number,
	category: {
		type: Schema.Types.ObjectId,
		ref: 'Categories'
	},
	logo: {
		type: Schema.Types.ObjectId,
		ref: 'Image'
	},
	featured: Boolean,
	menu: [
		{
			title: String,
			dishes: [
				{
					name: String,
					description: String,
					img: {
						type: Schema.Types.ObjectId,
						ref: 'Image'
					},
					price: Number
				}
			]
		}
	],
	workingTime:{
		monday: {
			open: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			},
			closed: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			}
		},
		tuesday: {
			open: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			},
			closed: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			}
		},
		wednesday: {
			open: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			},
			closed: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			}
		},
		thursday: {
			open: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			},
			closed: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			}
		},
		friday: {
			open: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			},
			closed: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			}
		},
		saturday: {
			open: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			},
			closed: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			}
		},
		sunday: {
			open: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			},
			closed: {
				type: Date,
				default: new Date(1989, 5, 13, 0, 0)
			}
		}
	}
}, {timestamps: true});

export default mongoose.model('Restaurant', RestaurantSchema);
