'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport('smtps://callcenter%40todomicilio.com.co:cambio09@smtp.zoho.com');
var Mustache = require('mustache');
const fs = require('fs');
var accounting = require('accounting');


var Schema = mongoose.Schema;
var OrderSchema = new mongoose.Schema({
	restaurant: {
		type: Schema.Types.ObjectId,
		ref: 'Restaurant'
	},
	customer: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	deliveryAddress: {
		description: String,
		streetType: String,
		num1: String,
		num2: String,
		num3: String,
		address2: String,
		phone: String
	},
	dishes: [
		{
			name: String,
			description: String,
			price: Number
		}
	],
	paymentMethod: String,
	deliveryCharge: Number,
	total: Number,
	subTotal: Number,
	status: {
		type: String,
		default: 'Pending'
	}
}, {timestamps: true});

var Order = mongoose.model('Order', OrderSchema)
export default Order;

OrderSchema.pre('save', function(next) {
	var order = this;

	Order.findById(order._id)
	.populate('restaurant')
	.populate('customer')
	.then(function(order){
		var order = order.toObject();

		order.deliveryCharge = accounting.formatMoney(order.deliveryCharge, "$", 0, ".", ".");
		order.subTotal = accounting.formatMoney(order.subTotal, "$", 0, ".", ".");
		order.total = accounting.formatMoney(order.total, "$", 0, ".", ".");

		order.dishes.forEach(function(dish, i){
			dish.price = accounting.formatMoney(order.dishes[i].price, "$", 0, ".", ".");
		});

		if(order.status === 'Confirmed'){
			fs.readFile(__dirname + '/../../views/mail/newOrderCustomerConf.html', 'utf8', function(err, template){
				var output = Mustache.render(template, order);

				// setup e-mail data with unicode symbols
				var mailOptions = {
					from: 'toDomicilio <callcenter@todomicilio.com.co>', // sender address
					to: order.customer.email, // list of receivers
					subject: 'Tu pedido en '+order.restaurant.name+' # '+order._id.toString().substring(20).toUpperCase()+' ha sido confirmado ✔', // Subject line
					html: output // html body
				};

				// send mail with defined transport object
				transporter.sendMail(mailOptions, function(error, info){
					if(error){
						return console.log(error);
					}
					console.log('Message sent: ' + info.response);
				});
			});
		}else if(order.status === 'Pending' && order.__v === 0){
			fs.readFile(__dirname + '/../../views/mail/newOrderCustomer.html', 'utf8', function(err, template){
				var output = Mustache.render(template, order);

				// setup e-mail data with unicode symbols
				var mailOptions = {
					from: 'toDomicilio <callcenter@todomicilio.com.co>', // sender address
					to: order.customer.email, // list of receivers
					subject: 'Tu pedido en '+order.restaurant.name+' # '+order._id.toString().substring(20).toUpperCase()+' ha sido recibido', // Subject line
					html: output // html body
				};

				// send mail with defined transport object
				transporter.sendMail(mailOptions, function(error, info){
					if(error){
						return console.log(error);
					}
					console.log('Message sent: ' + info.response);
				});
			});

			fs.readFile(__dirname + '/../../views/mail/newOrderAdmin.html', 'utf8', function(err, template){
				var output = Mustache.render(template, order);

				// setup e-mail data with unicode symbols
				var mailOptions = {
					from: 'toDomicilio <callcenter@todomicilio.com.co>', // sender address
					to: 'callcenter@todomicilio.com.co', // list of receivers
					subject: 'Nuevo pedido para '+order.restaurant.name+' # '+order._id.toString().substring(20).toUpperCase(), // Subject line
					html: output // html body
				};

				// send mail with defined transport object
				transporter.sendMail(mailOptions, function(error, info){
					if(error){
						return console.log(error);
					}
					console.log('Message sent: ' + info.response);
				});
			});
		}
	});

	
	next();
});

