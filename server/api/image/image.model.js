'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var ImageSchema = new mongoose.Schema({
  path: String,
});

export default mongoose.model('Image', ImageSchema);
