'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport('smtps://callcenter%40todomicilio.com.co:cambio09@smtp.zoho.com');
var Mustache = require('mustache');
const fs = require('fs');

var MessageSchema = new mongoose.Schema({
  name: String,
  company: String,
  nit: String,
  address: String,
  phone: String,
  email: String,
  message: String
});

MessageSchema.pre('save', function(next) {
  var message = this;

  fs.readFile(__dirname + '/../../views/mail/newMessage.html', 'utf8', function(err, template){
    var output = Mustache.render(template, message);

    // setup e-mail data with unicode symbols
    var mailOptions = {
      from: 'todomicilio.com.co <callcenter@todomicilio.com.co>', // sender address
      to: 'gbeltran@todomicilio.com.co', // list of receivers
      subject: 'Nuevo mensaje de ' + message.name + ' desde todomicilio.com.co', // Subject line
      html: output // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
      if(error){
        return console.log(error);
      }
      console.log('Message sent: ' + info.response);
    });
  });

  next();
});

export default mongoose.model('Message', MessageSchema);
